export default class ComponentClass {
  constructor (
    args = {
      name: 'no name'
    }) {
    this.name = args.name
  }

  calcAge () {
    return this.name.length
  }
}
