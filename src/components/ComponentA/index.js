import getElementById from './getElementById'
import './styles/main.scss'
import doc from './doc.block'
const renderLogo = (elementId) => {
  console.log(doc)
  const logo = require('./images/logo.png')
  let element = getElementById(elementId)
  element.innerHTML = `<img class="logo" src="${logo}">`
}
export {
  renderLogo
}
