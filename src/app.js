import ComponentB from './components/ComponentB'
import ComponentFunctional from './components/ComponentFunctional'
import ComponentClass from './components/ComponentClass'

let componentClassInstance = new ComponentClass({name: 'Duoc Nguyen'})
console.log(componentClassInstance.calcAge())

ComponentFunctional()

ComponentB.funcB()
window.onload = async () => {
  const renderLogo = await import(/* webpackChunkName: "ComponentA" */ './components/ComponentA')
  renderLogo.renderLogo('logo')
}
